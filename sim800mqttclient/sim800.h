

#ifndef sim800_h
#define sim800_h

#if (Arduino>=100)
#include "Arduino.h"
#endif
#include <EEPROM.h>

class sim800
{
  public:
    //constructor
    sim800();
    //methods
    void begin(long int baudRate);
    int resetModem(int);
    int initTCP(const char* __APN, const char* __usrnm, const char* __password, const char* MQTTHost, const char* MQTTPort, int sim800lresetPin) ;
    unsigned int readServerResponse(const char* ATcommand, unsigned int timeout);
    int MQTTpublish(const char* MQTTTopic, const char* message);
    int MQTTConnect(const char *MQTTClientID, char *MQTTUsername, const char *MQTTPassword, char MQTTFlags);
    int reconnectTcp(void);
    char aux_str[200];
    int MQTTsubscribe(const char* subTopic);
    bool sendSms(char *number , char *message);
    int closeTCP();
    int getSMSCommand();
    bool has(char *haystack, char *needle);
    int hasStringUpToIndex(char *haystack, char *needle);

    struct settings {
      int startingAddress;// sets the eeprom address from which to bwgin reading
      unsigned char isConfigured;// flag for configuration
      char* _value;// stores the phone number of the various contacts
    };
    
    settings server        = {80, 50, "193.70.38.71"};
    settings serverPort    = {200, 51, "1883"};
    settings deviceId      = {320, 52, "TestDevice"};
    settings admin1        = {400, 53, "0724540103"};
    settings gprsUsername  = {500, 56, "saf"};
    settings gprsPassword  = {600, 57, "data"};
    settings gprsApn       = {700, 58, "safaricom"};







    void  readSMS(int location);
    void updateFromEprom();
    int mqttPing();


    int  NewSMSpresent();
    bool newSMScommandPresent = false;



  private:
    unsigned int  sendATcommand2(const char* ATcommand, const char* expected_answer1, const char* expected_answer2, unsigned int timeout);
    unsigned int sendATcommand(const char* ATcommand, const char* expected_answer, unsigned int timeout);
    int8_t sim800::sendATcommand3(char* ATcommand, char* expected_answer1, unsigned long timeout);
    void readATString(bool watchTimeout = true);
    const char MQTTQOS = 0x00;
    const char MQTTPacketID = 0x0001;
    unsigned char encodedByte;
    int X;
    unsigned long datalength;
    unsigned short topiclength2;
    char str[250];
    const char * MQTTTopic;
    unsigned char topic[30];
    unsigned short topiclength;
    const unsigned int MQTTKeepAlive = 60;
    const char * MQTTProtocolName = "MQTT";
    //const char * MQTTProtocolName = "MQIsdp";

    //const char MQTTLVL = 0x03;
    const char MQTTLVL = 0x04;

    unsigned short MQTTProtocolNameLength;
    unsigned short MQTTClientIDLength;
    unsigned short MQTTUsernameLength;
    unsigned short MQTTPasswordLength;
    unsigned short MQTTTopicLength;
};
#endif
