#include <Metro.h>
#include <TinyGPS++.h>
#include "sim800.h"
#include <Filters.h>
#include <stdio.h>
//#define dbg
//#define NYABURURU
//#define SUNEKA
#define MILIMANI
//#define ISANA





float testFrequency = 50;
float windowLength = 20.0;
int sensorValue = 0;
float intercept = -0.1329;
float slope = 0.0405;
float current_amps;
float current_Amps = 10.001;
unsigned long printPeriod = 1000;
unsigned long previousMillis = 0;


Metro newSMSinterval = Metro(10000);
Metro publishInterval = Metro(300000);
Metro pingInterval = Metro(60000);
Metro statusLed = Metro(1000);
Metro smsInterval = Metro(10000);
Metro dailyReset = Metro(86400000);

char  current[10];
float Pressure;
int BatteryCapacity;
bool mainsNotAvailable;
bool batteryLow;

sim800 myModem;
TinyGPSPlus gps;

static const uint32_t GPSBaud = 9600;
//GPS data
char *_date;
char *_time;
int satelites;
float longitude;
float latitude;
int speedKph;
unsigned long int previous;

char longitudeString[20];
char latitudeString[20];
char * locations;

int  previousState = 0;
int  currentState = 0;
bool changeOfStateFlag = 0;
bool TCPconnection = 0;

const char * MQTTClientID = "ABCDEF";
char MQTTHost[20];
char MQTTPort[20];
char admin[20];

char  APN[20];
char  USERNAME[20];
char  PASSWORD[20];

char  MQTTUsername [40];
//"Px5tQMqJzVApFVoxChAw";
const char * MQTTPassword = "";

const char MQTTFlags = 0xC2;
int sim800ResetPin = 4;

char  messageZ[200];
char sms[200];


const char * telemetryTopic = "v1/devices/me/telemetry";
const char * attributesTopic = "v1/devices/me/attributes";
const char * attributesRequestTopic = "v1/devices/me/attributes/request/1";
const char * SubscribeTopic = "v1/devices/me/rpc/request/+";

//const char * attributes = "{\"FirmWareVersion\":\"0\",\"DeviceName\":\"Pressure Point\",\"DeviceLocation\":\"Nyabururu\"}";
const char * attributes = "{\"FirmWareVersion\":\"0\",\"DeviceName\":\"Pressure Point\",\"DeviceLocation\":\"Point 2\"}";


unsigned int StatusLedError = 13;
unsigned int StatusLedOk = 11;
int siteRebootControl = 13;


int SystemState;



void setup() {
  Serial.begin(115200);
  Serial.println("BLINK MODEM 1");
  myModem.begin(115200);
  Serial3.begin(GPSBaud);

  pinMode(21, INPUT_PULLUP);
  pinMode(StatusLedOk, OUTPUT);
  pinMode(StatusLedError, OUTPUT);

  delay(1000);
}

void loop() {
  updateFromEprom();

  digitalWrite(StatusLedOk, HIGH);
  digitalWrite(StatusLedError, HIGH);
  processSMS();

  if (myModem.initTCP(APN, USERNAME, PASSWORD, MQTTHost, MQTTPort, sim800ResetPin)) {
    TCPconnection = true;
    errorIndicator(1);
    delay(1000);
    Serial.println("TCP connection estsblished");
    processSMS();
    if (myModem.MQTTConnect(MQTTClientID, MQTTUsername, MQTTPassword, MQTTFlags)) {
      Serial.println("MQ Connection established");
      errorIndicator(1);
      //      if (myModem.MQTTsubscribe(SubscribeTopic))Serial.println("BOOOOOOOOOOOOOOOM");
      //      else (Serial.println("NAY"));

      if (myModem.MQTTpublish(attributesTopic, attributes)) {
        Serial.println("Publish attributes success");
        errorIndicator(1);
        delay(300);
      }
      else {
        Serial.println("Publish attributes Fail");
        errorIndicator(0);
      }

      while (1) {
        digitalWrite(StatusLedOk, HIGH);
        digitalWrite(StatusLedError, LOW);
        eventsProcessor();

        if (smsInterval.check())processSMS();

        if (statusLed.check())digitalWrite(StatusLedOk, LOW);

        if (publishInterval.check()) {
          if (myModem.MQTTpublish(telemetryTopic, messageZ)) {
            Serial.println("Publish success");
            errorIndicator(1);
          }
          else {
            Serial.println("Telemetry publish fail");
            errorIndicator(0);
          }
        }

        if (pingInterval.check()) {
          if (myModem.mqttPing())Serial.println("PING OK");
          else {
            Serial.println("PING FAIL");
            errorIndicator(0);
          }
        }
        if (dailyReset.check()) {
          errorIndicator(0);
          //myModem.sendSms(admin, "Daily Reset");
        }
        if (!SystemState)break;
      }
    }
    else {
      Serial.println("MQTT connect fail");
      errorIndicator(0);
    }
  }
  else {
    TCPconnection = false;
    Serial.println("TCP connect fail");
    eventsProcessor();
    errorIndicator(0);
  }
}

void errorIndicator(int state) {
  SystemState = state;
  if (state) {
    digitalWrite(StatusLedOk, HIGH);
    digitalWrite(StatusLedError, LOW);
  }
  else {
    digitalWrite(StatusLedOk, LOW);
    digitalWrite(StatusLedError, HIGH);
  }
}
void eventsProcessor() {
  measure();
  char _buffer[5];
  dtostrf(Pressure, 3, 1, _buffer);

  if (Pressure > 10)currentState = 1;


  if (TCPconnection)
  {
#ifdef ISANA
    //-0.67, 34.70
    sprintf(messageZ, "{\"Pressure\":\"%s\",\"longitude\":\"34.70\",\"latitude\":\"-0.67\",\"Connection\":\"ONLINE\"}", _buffer);//MILIMANI
    sprintf(sms, "ISANA\nPressure:%s\nhttp://maps.google.com/maps?q=-0.67,34.70\nGPRS Connection:ONLINE", _buffer);
#endif



#ifdef SUNEKA
    //-0.678492, 34.707021
    sprintf(messageZ, "{\"Pressure\":\"%s\",\"longitude\":\"34.707021\",\"latitude\":\"-0.678492\",\"Connection\":\"ONLINE\"}", _buffer);//MILIMANI
    sprintf(sms, "SUNEKA\nPressure:%s\nhttp://maps.google.com/maps?q=-0.678492,34.707021\nGPRS Connection:ONLINE", _buffer);
#endif

#ifdef MILIMANI
    sprintf(messageZ, "{\"Pressure\":\"%s\",\"longitude\":\"34.752683\",\"latitude\":\"-0.722091\",\"Connection\":\"ONLINE\"}", _buffer);//MILIMANI
    sprintf(sms, "MILIMANI\nPressure:%s\nhttp://maps.google.com/maps?q=-0.722091,34.752683\nGPRS Connection:ONLINE", _buffer);

#endif

#ifdef NYABURURU
    sprintf(messageZ, "{\"Pressure\":\"%s\",\"longitude\":\"34.748563\",\"latitude\":\"-0.647938\",\"Connection\":\"ONLINE\"}", _buffer);//MILIMANI
    sprintf(sms, "NYABURURU\nPressure:%s\nhttp://maps.google.com/maps?q=-0.647938,34.748563\nGPRS Connection:ONLINE", _buffer);
#endif

    currentState = 2;
  }
  else {
#ifdef ISANA
    //-0.67, 34.70
    sprintf(messageZ, "{\"Pressure\":\"%s\",\"longitude\":\"34.75\",\"latitude\":\"-0.67\",\"Connection\":\"OFFLINE\"}", _buffer);//MILIMANI
    sprintf(sms, "ISANA\nPressure:%s\nhttp://maps.google.com/maps?q=-0.67,34.70,\nGPRS Connection:OFFLINE", _buffer);
#endif
#ifdef SUNEKA
    //-0.678492, 34.707021
    sprintf(messageZ, "{\"Pressure\":\"%s\",\"longitude\":\"34.752683\",\"latitude\":\"-0.678492\",\"Connection\":\"OFFLINE\"}", _buffer);//MILIMANI
    sprintf(sms, "SUNEKA\nPressure:%s\nhttp://maps.google.com/maps?q=-0.678492,34.707021,\nGPRS Connection:OFFLINE", _buffer);
#endif


#ifdef MILIMANI
    sprintf(messageZ, "{\"Pressure\":\"%s\",\"longitude\":\"34.752683\",\"latitude\":\"-0.722091\",\"Connection\":\"OFFLINE\"}", _buffer);//MILIMANI
    sprintf(sms, "MILIMANI\nCurrent:%s\nPressure:%s\nhttp://maps.google.com/maps?q=-0.722091,34.752683,\nGPRS Connection:OFFLINE", _buffer);
#endif

#ifdef NYABURURU
    sprintf(messageZ, "{\"Pressure\":\"%s\",\"longitude\":\"34.748563\",\"latitude\":\"-0.647938\",\"Connection\":\"OFFLINE\"}", _buffer);//MILIMANI
    sprintf(sms, "NYABURURU\nCurrent:%s\nPressure:%s\nhttp://maps.google.com/maps?q=-0.647938,34.748563,\nGPRS Connection:OFFLINE", _buffer);

#endif

    currentState = 3;
  }


  Serial.println(messageZ);

  if (currentState != previousState) {

    Serial.print("EVENT DETECTED");
    Serial.println ("\t");
    Serial.println (currentState);
    if (myModem.MQTTpublish(telemetryTopic, messageZ)) {
      Serial.println("Publish success");
      errorIndicator(1);
      previousState = currentState ;
    }
    if (myModem.sendSms(admin, sms)) {
      previousState = currentState ;
      errorIndicator(1);
    }
    else {
      Serial.println("SMS Sending failed");
      errorIndicator(0);
    }

  }
  Serial.println(sms);

}
void measure() {
  int analog7;
  analog7 = analogRead(A7);
  //FOR A 4mA to 20mA pressure sensor mapping pressure to 0 to 20
  Pressure = ((10 * analog7 / 349) - 173); //MILIMANI CALLIBRATION RESISTANCE 213 OHMS
  while ( true ) {

    analog7 = analogRead(A7);
    Pressure = (0.9 * Pressure) + (0.1 * ((10 * analog7 / 349) - 173));
    if ((unsigned long)(millis() - previousMillis) >= printPeriod) {
      previousMillis = millis();   // update time
      break;
    }
  }
  if (Pressure < 0)Pressure = 0;

  Serial.println(Pressure, 2);
}


void processSMS() {
  int answer;
  if (newSMSinterval.check() == 1) {
    int smsIndex = 1;
    while (myModem.NewSMSpresent()) {
      myModem.readSMS(smsIndex);
      answer = myModem.getSMSCommand();
      if (answer == 100) {
        myModem.sendSms(admin, sms);
      }
      else if (answer == 1)updateFromEprom(); //update parameters
      smsIndex++;
      if (smsIndex == 26)break;
    }

  }
}


//void measure() {
//
//
//  int analog8;
//  analog8 = analogRead(A8);
//  //  Pressure = 0.9((Pressure * 0.0214) - 0.8) + 0.1(voltageRaw);
//  //FOR A 4mA to 20mA pressure sensor mapping pressure to 0 to 20
//  Pressure = analog8/45;
//  mainsNotAvailable = digitalRead(21);
//
//  RunningStatistics inputStats;
//  inputStats.setWindowSecs( windowLength );
//  while ( true ) {
//    sensorValue = analogRead(A6);  // read the analog in value:
//    inputStats.input(sensorValue);  // log to Stats function
//
//    analog8 = analogRead(A7);
//    Pressure = 0.9 * Pressure + (0.1 * analog8/45);
//
//    if ((unsigned long)(millis() - previousMillis) >= printPeriod) {
//      previousMillis = millis();   // update time
//
//      // display current values to the screen
//      //Serial.print( "\n" );
//      // output sigma or variation values associated with the inputValue itsel
//      //Serial.print( "\tsigma: " ); Serial.print( inputStats.sigma() );
//      // convert signal sigma value to current in amps
//      current_amps = intercept + 0.1 + slope * inputStats.sigma();
//      //Serial.print( "\tamps: " ); Serial.print( current_amps );
//      dtostrf(current_amps, 3, 1, current);
//      //Serial.print( "\tamps: " ); Serial.println(current);
//      break;
//    }



