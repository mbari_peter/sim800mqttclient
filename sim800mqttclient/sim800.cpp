#include "sim800.h"
#include "arduino.h"
#include <stdio.h>


#define dbg
//#define transparentMode

sim800::sim800() {
}

void sim800::begin(long int baudRate) {
  Serial2.begin(baudRate);
  delay(2000);
  Serial2.println("+++");
}

unsigned int  sim800::sendATcommand2(const char* ATcommand, const  char* expected_answer1, const char* expected_answer2, unsigned int timeout) {
  uint8_t x = 0,  answer = 0;
  char response[100];
  unsigned long previous;

  memset(response, '\0', 100);    // Initialize the string

  delay(1000);

  Serial2.flush();
  //while(Serial2.available())Serial.write(Serial2.read());
  Serial2.println(ATcommand);    // Send the AT command
  //if(strstr(ATcommand, "AT+CIPSEND")!=NULL) Serial2.write(0x1A);

#ifdef dbg
  Serial.println(ATcommand);    // Send the AT command
#endif

  x = 0;
  previous = millis();

  // this loop waits for the answer
  do {
    // if there are data in the UART input buffer, reads it and checks for the asnwer
    if (Serial2.available() != 0) {
      response[x] = Serial2.read();
      x++;
      // check if the desired answer 1  is in the response of the module
      if (strstr(response, expected_answer1) != NULL)
      {
        answer = 1;
        while (Serial.available()) {
          response[x] = Serial2.read();
          x++;
        }
      }
      // check if the desired answer 2 is in the response of the module
      else if (strstr(response, expected_answer2) != NULL)
      {
        answer = 2;
        while (Serial.available()) {
          response[x] = Serial2.read();
          x++;
        }
      }

    }
  }
  // Waits for the asnwer with time out
  while ((answer == 0) && ((millis() - previous) < timeout));
#ifdef dbg
  Serial.println(response);
  while (Serial2.available())Serial.write(Serial2.read());

#endif

  return answer;
}

unsigned int  sim800::sendATcommand(const char* ATcommand, const char* expected_answer, unsigned int timeout) {

  uint8_t x = 0,  answer = 0;
  char response[500];
  unsigned long previous;
  memset(response, '\0', 100);    // Initialize the string

  delay(1000);

  while ( Serial2.available() > 0) Serial2.read();   // Clean the input buffer
  //while(Serial2.available())Serial.write(Serial2.read());

  Serial2.println(ATcommand);    // Send the AT command
#ifdef dbg
  Serial.println(ATcommand);    // Send the AT command
#endif


  x = 0;
  previous = millis();

  // this loop waits for the answer
  do {
    if (Serial2.available() != 0) {
      // if there are data in the UART input buffer, reads it and checks for the asnwer
      response[x] = Serial2.read();
      x++;
      // check if the desired answer  is in the response of the module
      if (strstr(response, expected_answer) != NULL)
      {
        answer = 1;

      }
    }
  }
  // Waits for the asnwer with time out
  while ((answer == 0) && ((millis() - previous) < timeout));

#ifdef dbg
  Serial.println(response);
  while (Serial2.available())Serial.write(Serial2.read());

#endif
  return answer;
}

int sim800::initTCP(const char* __APN, const char* __usrnm, const char* __password, const char* MQTTHost, const char* MQTTPort, int sim800lresetPin) {

  int answer = 0;
  resetModem(sim800lresetPin);
  if (sendATcommand2("ATE0", "OK", "ERROR", 2000)) {
    sendATcommand("AT&W", "OK", 2000);
  }
  delay(2000);
  Serial2.println("+++");
  Serial.println(F("Connecting . . . ."));
  delay(2000);
  while ( sendATcommand2("AT+CREG?", "+CREG: 0,1", "+CREG: 0,5", 1000) == 0 );
  Serial.println(F("Connected"));
  delay(2000);

  sendATcommand("AT+IPR=115200", "OK", 1000);    // Selects Single-connection mode
  delay(2000);
  sendATcommand("AT+CMGF=1", "OK", 1000);    // sets the SMS mode to text
  sendATcommand("AT+CPMS=\"SM\",\"SM\",\"SM\"", "OK", 1000);    // selects the memory


  if (sendATcommand2("AT+CIPMUX=0", "OK", "ERROR", 1000) == 1)      // Selects Single-connection mode

  {

    while ((sendATcommand2("AT+CIPMODE=0", "OK", "ERROR", 1000) ) == 2); //srt non transparent mode for data sending
    delay(1000);

    if (!(sendATcommand2("AT+CIPSRIP=0", "OK", "ERROR", 1000) ))answer = 0; //Do not show the prompt during receiving data from server
    delay(1000);
    if (sendATcommand2("AT+CIPRXGET=1", "OK", "ERROR", 1000) == 1) { //RECEIVE DATA manually FROM THE REMOTE SERVER
      delay(1000);


      while (sendATcommand("AT+CGATT?", "+CGATT: 1", 5000) == 0 );
      delay(1000);

      // Waits for status IP INITIAL
      while (sendATcommand("AT+CIPSTATUS", "INITIAL", 5000) == 0 );
      delay(1000);

      snprintf(aux_str, sizeof(aux_str), "AT+CSTT=\"%s\",\"%s\",\"%s\"", __APN, __usrnm, __password);

      // Sets the APN, user name and password
      if (sendATcommand2(aux_str, "OK",  "ERROR", 30000) == 1)
      {

        // Waits for status IP START
        if (sendATcommand("AT+CIPSTATUS", "START", 500)  == 0 )
          delay(3000);

        // Brings Up Wireless Connection
        if (sendATcommand2("AT+CIICR", "OK", "ERROR", 30000) == 1)
        {

          // Waits for status IP GPRSACT
          while (sendATcommand("AT+CIPSTATUS", "GPRSACT", 500)  == 0 );
          delay(3000);

          // Gets Local IP Address
          if (sendATcommand2("AT+CIFSR", ".", "ERROR", 10000) == 1)
          {

            // Waits for status IP STATUS
            while (sendATcommand("AT+CIPSTATUS", "IP STATUS", 500)  == 0 );
            delay(5000);

            Serial.println(F("Connecting TCP"));
            snprintf(aux_str, sizeof(aux_str), "AT+CIPSTART=\"TCP\",\"%s\",\"%s\"", MQTTHost, MQTTPort);

            // Opens a TCP socket
            if (sendATcommand2(aux_str, "OK\r\n\r\nCONNECT", "CONNECT FAIL", 30000) == 1)
            {
              answer = 1;
            }

            else
            {
              Serial.println(F("Error opening the connection"));
              Serial.println(F("UNABLE TO CONNECT TO SERVER "));
              answer = 0;
            }
          }
          else
          {
            Serial.println(F("ERROR GETTING IP ADDRESS "));
            answer = 0;

          }
        }
        else
        {
          Serial.println(F("ERROR BRINGING UP WIRELESS CONNECTION"));
          answer = 0;
        }
      }
      else {
        Serial.println(F("Error setting the APN"));
        answer = 0;
      }


    }
    else
    {
      Serial.println(F("Error setting CIPRXGET"));
      answer = 0;
    }


  }
  return answer;
}



int  sim800::MQTTConnect(const char *MQTTClientID, char *MQTTUsername, const char *MQTTPassword, char MQTTFlags) {
  int answer = 0;
  char CONNACK = 2;
  char response[100];
  char  index = 0;
  delay(5000);
  answer = sendATcommand2("AT+CIPSEND", ">", "ERROR", 1000);

  if (answer) {

    Serial2.write(0x10);
    MQTTProtocolNameLength = strlen(MQTTProtocolName);
    MQTTClientIDLength = strlen(MQTTClientID);
    MQTTUsernameLength = strlen(MQTTUsername);
    MQTTPasswordLength = strlen(MQTTPassword);
    datalength = MQTTProtocolNameLength + 2 + 4 + MQTTClientIDLength + 2 + MQTTUsernameLength + 2 + MQTTPasswordLength + 2;
    X = datalength;
    do
    {
      encodedByte = X % 128;
      X = X / 128;
      // if there are more data to encode, set the top bit of this byte
      if ( X > 0 ) {
        encodedByte |= 128;
      }

      Serial2.write(encodedByte);
    }
    while ( X > 0 );
    Serial2.write(MQTTProtocolNameLength >> 8);
    Serial2.write(MQTTProtocolNameLength & 0xFF);
    Serial2.write(MQTTProtocolName);

    Serial2.write(MQTTLVL); // LVL
    Serial2.write(MQTTFlags); // Flags
    Serial2.write(MQTTKeepAlive >> 8);
    Serial2.write(MQTTKeepAlive & 0xFF);


    Serial2.write(MQTTClientIDLength >> 8);
    Serial2.write(MQTTClientIDLength & 0xFF);
    Serial2.print(MQTTClientID);


    Serial2.write(MQTTUsernameLength >> 8);
    Serial2.write(MQTTUsernameLength & 0xFF);
    Serial2.print(MQTTUsername);


    Serial2.write(MQTTPasswordLength >> 8);
    Serial2.write(MQTTPasswordLength & 0xFF);
    Serial2.print(MQTTPassword);


    Serial2.write(0x1A);
    if (sendATcommand2("", "SEND OK", "SEND FAIL", 20000))  answer = 1;
    delay(2000);
    Serial2.flush();
    Serial2.println("AT+CIPRXGET=2,1024");
    delay(2000);

    if (Serial2.available()) {
      while (Serial2.available()) {
        response[index] = Serial2.read();
        index++;
        Serial.print(response[index]);
        if (response[index] == CONNACK) {
          Serial.println("CONNACK");
          answer = 1;
        }
        // else answer = 0;
      }
    }
  }
  return answer;
}

int sim800::MQTTsubscribe(const char * MQTTTopic2) {
  int answer = 0;
  if (sendATcommand2("AT+CIPSEND", ">", "ERROR", 2000)) {

    memset(str, 0, 250);
    topiclength2 = strlen(MQTTTopic2);
    datalength = 2 + 2 + topiclength2 + 1;
    delay(1000);

    Serial2.write(0x82);
    X = datalength;
    do
    {
      encodedByte = X % 128;
      X = X / 128;
      // if there are more data to encode, set the top bit of this byte
      if ( X > 0 ) {
        encodedByte |= 128;
      }
      Serial2.write(encodedByte);
    }
    while ( X > 0 );
    Serial2.write(MQTTPacketID >> 8);
    Serial2.write(MQTTPacketID & 0xFF);
    Serial2.write(topiclength2 >> 8);
    Serial2.write(topiclength2 & 0xFF);
    Serial2.print(MQTTTopic2);
    Serial2.write(MQTTQOS);

    Serial2.write(0x1A);
    if (sendATcommand2("", "SEND OK", "SEND FAIL", 10000)) {
      Serial.println(F("SUBSCRIBE SUCCESSFUL"));
      answer = 1;
    }

  }

}

int  sim800::MQTTpublish(const char* MQTTTopic, const char* message) {
  int answer = 0;
  char PUBACK = 4;
  char index = 0;
  char response[100];
  delay(5000);


  answer = sendATcommand2("AT+CIPSEND", ">", "ERROR", 1000);


  if (answer) {

    memset(str, 0, sizeof(str));
    topiclength = strlen(MQTTTopic);
    datalength = sprintf(str, "%s%s", MQTTTopic, message);

    delay(1000);
    Serial2.write(0x30);
    X = datalength + 2;
    do
    {
      encodedByte = X % 128;
      X = X / 128;
      // if there are more data to encode, set the top bit of this byte
      if ( X > 0 ) {
        encodedByte |= 128;
      }
      Serial2.write(encodedByte);
    }
    while ( X > 0 );

    Serial2.write(topiclength >> 8);
    Serial2.write(topiclength & 0xFF);
    Serial2.print(str);

    Serial2.write(0x1A);

    if (sendATcommand2("", "SEND OK", "SEND FAIL", 20000))  answer = 1;
    else answer = 0;
    delay(2000);
    Serial2.flush();
    Serial2.println("AT+CIPRXGET=2,1024");
    delay(2000);

    if (Serial2.available()) {
      while (Serial2.available()) {
        response[index] = Serial2.read();
        index++;
        //  Serial.print(response[index]);
        if (response[index] == PUBACK) {
          Serial.println("PUBACK");
          answer = 1;
        }
        // else answer = 0;
      }
    }
  }
  return answer;
}


int sim800::mqttPing() {
  char response[100];
  char index = 0;
  char PINGRESP = 13;
  int answer = 0;
  if (sendATcommand2("AT+CIPSEND", ">", "ERROR", 1000)) {
    Serial2.write(0xC0);
    Serial2.write(0x00);
    Serial2.write(0x1A);

    delay(4000);
    Serial2.flush();
    Serial2.println("AT+CIPRXGET=2,1024");
    delay(2000);

    if (Serial2.available()) {
      while (Serial2.available()) {
        response[index] = Serial2.read();
        index++;
        if (response[index] == PINGRESP) {
          //Serial.println("PINGRSP");
          answer = 1;
          break;
        }
        else answer = 0;
      }
    }
  }
  return (answer);

}


int sim800::resetModem(int sim800lreset) {
  bool answer = 0;
  pinMode(sim800lreset, OUTPUT);
  digitalWrite(sim800lreset, LOW);
  delay(1000);
  digitalWrite(sim800lreset, HIGH);
  // checks if the module is started
  delay(3000);
  answer = sendATcommand("AT", "OK", 2000);
  if (answer == 1) {
    sendATcommand("ATE0", "OK", 2000);
#ifdef dbg
    Serial.println("MODEM STARTED");
#endif
    return 1; // turn off the echo
  }
  else if (answer == 0)
  {
    // power on pulse
    digitalWrite(sim800lreset, LOW);
    delay(1000);
    digitalWrite(sim800lreset, HIGH);
    delay(1000);
    // waits for an answer from the module
    int trials = 0;
    while (answer == 0) {
      trials++;
      // Send AT every two seconds and wait for the answer
      answer = sendATcommand("AT", "OK", 2000);
      if (answer)return true;
      if (trials == 5) {
#ifdef dbg
        Serial.println(F("Gsm Start Fail"));
#endif
        answer = 0;
      }
    }
  }
  return answer;
}

unsigned int sim800::readServerResponse(const char* ATcommand, unsigned int timeout) {
  unsigned int  answer;
  Serial2.println(ATcommand);
  delay(4000);
  while (Serial2.available())Serial.write(Serial2.read());
  return answer;
}

bool sim800::sendSms(char* number, char* message) {

  delay(3000);
  int answer;
  sendATcommand("AT+CMGF=1", "OK", 1000);    // sets the SMS mode to text
#ifdef dbg
  Serial.println(F("Sending SMS"));
#endif
  memset(aux_str, '\0', sizeof(aux_str));
  sprintf(aux_str, "AT+CMGS=\"%s\"", number);
  answer = sendATcommand(aux_str, ">", 2000);    // send the SMS number
  if (answer == 1)
  {
    Serial2.println(message);
    Serial2.write(0x1A);
    answer = sendATcommand("", "OK", 20000);
    if (answer == 1)
    {
#ifdef dbg
      Serial.println("Sent ");
#endif
      //AIRTIMEERROR=false;
      return true;
    }
    else
    {
      Serial.println("error ");
      // AIRTIMEERROR=true;
    }
  }
  else
  {
    Serial.print("error ");
    Serial.println(answer, DEC);
    return false;
  }
}

int sim800::closeTCP() {
  return sendATcommand("AT+CIPSHUT", "OK", 10000);
}


bool sim800::has(char *haystack, char *needle)
{
  int compareOffset = 0;
  //iterate through the zero terminated string heystack
  while (*haystack) {
    if (*haystack == *needle) { //we might have found a match
      compareOffset = 0; //start at the current location in haystack
      //see if the string from that location matches the needle
      while (haystack[compareOffset] == needle[compareOffset]) {
        compareOffset++;
        if (needle[compareOffset] == '\0') {
          return true; //we have reached the end of needle and everything matched
        }
      }
    }
    haystack++; //increment the location in the string (the pointer)
  }
  return false; //no match was found
}


int sim800::hasStringUpToIndex(char *haystack, char *needle)
{
  int compareOffset = 0;
  int index = 0;
  //iterate through the zero terminated string heystack
  while (*haystack) {
    index ++;
    if (*haystack == *needle) { //we might have found a match
      compareOffset = 0; //start at the current location in haystack
      //see if the string from that location matches the needle
      while (haystack[compareOffset] == needle[compareOffset]) {
        compareOffset++;
        index++;
        if (needle[compareOffset] == '\0') {
          return index; //we have reached the end of needle and everything matched
        }
      }
    }
    haystack++; //increment the location in the string (the pointer)
  }
  return false; //no match was found
}


int sim800::getSMSCommand() {
  char sms[100];


  if (has(aux_str, "CONFIG")) {
    //config the (server.ipAdress) number via sms
    if (has(aux_str, "server")) {
      int index = hasStringUpToIndex(aux_str, "server"); //locate  the (server.ipAdress) config
      char stringValue[20];// string for storing the variable being sort.
      int stringIndex = 0;
      while (1) {
        stringValue[stringIndex] = aux_str[index];
        stringIndex++;
        index++;
        if ((index == sizeof(aux_str) | (aux_str[index] == '\0') | (aux_str[index] == ',') | (aux_str[index] == '*') | (aux_str[index] == '#') | (aux_str[index] == 'O'))) {
          stringValue[stringIndex] = '\0';
          break;
        }
      }
      Serial.print(F("server No updated  "));
      // Serial.println(stringValue);
      strcpy (server._value, stringValue);
      // Serial.print(F("server Number is "));
      Serial.println((server._value));
      //write this value to eeprom
      int WriteCounter;
      for (WriteCounter = 0; WriteCounter < stringIndex; WriteCounter++) {
        EEPROM.write(WriteCounter + server.startingAddress + 1, server._value[WriteCounter]);
      }
      EEPROM.write(WriteCounter + server.startingAddress + 1, '\0');
      EEPROM.write(server.startingAddress, '\0');
      int readIndex = 0;
#ifdef dbg
      Serial.print(F("reading from EEPROM "));
      for (readIndex = 0; readIndex < stringIndex; readIndex++) {
        char c =  EEPROM.read(server.startingAddress + 1 + readIndex);
        if (c == 0)Serial.print('0');
        else Serial.print(char(c));
      }
      Serial.println();
      Serial.println('\n');
#endif
      memset(sms, '\0', sizeof(sms));
      sprintf(sms, "server IP address SET to %s", server._value);
      sendSms(admin1._value, sms);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //config the (port._value) number via sms
    else if (has(aux_str, "port")) {
      int index = hasStringUpToIndex(aux_str, "port"); //locate  the (serverPort._value) config
      char stringValue[20];// string for storing the variable being sort.
      int stringIndex = 0;
      while (1) {
        stringValue[stringIndex] = aux_str[index];
        stringIndex++;
        index++;
        if ((index == sizeof(aux_str) | (aux_str[index] == '\0') | (aux_str[index] == ',') | (aux_str[index] == '*') | (aux_str[index] == '#') | (aux_str[index] == 'O'))) {
          stringValue[stringIndex] = '\0';
          break;
        }
      }
      Serial.print(F("serverPort No updated  "));
      // Serial.println(stringValue);
      strcpy (serverPort._value, stringValue);
      // Serial.print(F("serverPort Number is "));
      Serial.println((serverPort._value));
      //write this value to eeprom
      int WriteCounter;
      for (WriteCounter = 0; WriteCounter < stringIndex; WriteCounter++) {
        EEPROM.write(WriteCounter + serverPort.startingAddress + 1, serverPort._value[WriteCounter]);
      }
      EEPROM.write(WriteCounter + serverPort.startingAddress + 1, '\0');
      EEPROM.write(serverPort.startingAddress, '\0');
      int readIndex = 0;
#ifdef dbg
      Serial.print(F("reading from EEPROM "));
      for (readIndex = 0; readIndex < stringIndex; readIndex++) {
        char c =  EEPROM.read(serverPort.startingAddress + 1 + readIndex);
        if (c == 0)Serial.print('0');
        else Serial.print(char(c));
      }
      Serial.println();
      Serial.println('\n');
#endif
      memset(sms, '\0', sizeof(sms));
      sprintf(sms, "server port set to %s", serverPort._value);
      sendSms(admin1._value, sms);
    }
    //////////////////////////////////////////////////////////////////////////////////
    else if (has(aux_str, "deviceId")) {
      int index = hasStringUpToIndex(aux_str, "deviceId"); //locate  the (deviceId._value) config
      char stringValue[40];// string for storing the variable being sort.
      int stringIndex = 0;
      while (1) {
        stringValue[stringIndex] = aux_str[index];
        stringIndex++;
        index++;
        if ((index == sizeof(aux_str) | (aux_str[index] == '\0') | (aux_str[index] == ',') | (aux_str[index] == '*') | (aux_str[index] == '#') | (aux_str[index] == 'O'))) {
          stringValue[stringIndex] = '\0';
          break;
        }
      }
      Serial.print(F("deviceId No updated  "));
      // Serial.println(stringValue);
      strcpy (deviceId._value, stringValue);
      // Serial.print(F("deviceId Number is "));
      Serial.println((deviceId._value));
      //write this value to eeprom
      int WriteCounter;
      for (WriteCounter = 0; WriteCounter < stringIndex; WriteCounter++) {
        EEPROM.write(WriteCounter + deviceId.startingAddress + 1, deviceId._value[WriteCounter]);
      }
      EEPROM.write(WriteCounter + deviceId.startingAddress + 1, '\0');
      EEPROM.write(deviceId.startingAddress, '\0');
      int readIndex = 0;
#ifdef dbg
      Serial.print(F("reading from EEPROM "));
      for (readIndex = 0; readIndex < stringIndex; readIndex++) {
        char c =  EEPROM.read(deviceId.startingAddress + 1 + readIndex);
        if (c == 0)Serial.print('0');
        else Serial.print(char(c));
      }
      Serial.println();
      Serial.println('\n');
#endif
      memset(sms, '\0', sizeof(sms));
      sprintf(sms, "deviceId set to %s", deviceId._value);
      sendSms(admin1._value, sms);
    }

    /////////////////////////////////////////////////////////////////////////////////
    else if (has(aux_str, "admin1")) {
      int index = hasStringUpToIndex(aux_str, "admin1"); //locate  the (admin1._value) config
      char stringValue[20];// string for storing the variable being sort.
      int stringIndex = 0;
      while (1) {
        stringValue[stringIndex] = aux_str[index];
        stringIndex++;
        index++;
        if ((index == sizeof(aux_str) | (aux_str[index] == '\0') | (aux_str[index] == ',') | (aux_str[index] == '*') | (aux_str[index] == '#') | (aux_str[index] == 'O'))) {
          stringValue[stringIndex] = '\0';
          break;
        }
      }
      Serial.print(F("admin1 No updated  "));
      // Serial.println(stringValue);
      strcpy (admin1._value, stringValue);
      // Serial.print(F("admin1 Number is "));
      Serial.println((admin1._value));
      //write this value to eeprom
      int WriteCounter;
      for (WriteCounter = 0; WriteCounter < stringIndex; WriteCounter++) {
        EEPROM.write(WriteCounter + admin1.startingAddress + 1, admin1._value[WriteCounter]);
      }
      EEPROM.write(WriteCounter + admin1.startingAddress + 1, '\0');
      EEPROM.write(admin1.startingAddress, '\0');
      int readIndex = 0;
#ifdef dbg
      Serial.print(F("reading from EEPROM "));
      for (readIndex = 0; readIndex < stringIndex; readIndex++) {
        char c =  EEPROM.read(admin1.startingAddress + 1 + readIndex);
        if (c == 0)Serial.print('0');
        else Serial.print(char(c));
      }
      Serial.println();
      Serial.println('\n');
#endif
      memset(sms, '\0', sizeof(sms));
      sprintf(sms, "admin1 phone set to %s", admin1._value);
      sendSms(admin1._value, sms);
    }
    /////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////
    else if (has(aux_str, "gprsUsername")) {
      int index = hasStringUpToIndex(aux_str, "gprsUsername"); //locate  the (gprsUsername._value) config
      char stringValue[20];// string for storing the variable being sort.
      int stringIndex = 0;
      while (1) {
        stringValue[stringIndex] = aux_str[index];
        stringIndex++;
        index++;
        if ((index == sizeof(aux_str) | (aux_str[index] == '\0') | (aux_str[index] == ',') | (aux_str[index] == '*') | (aux_str[index] == '#') | (aux_str[index] == 'O'))) {
          stringValue[stringIndex] = '\0';
          break;
        }
      }
      Serial.print(F("gprsUsername No updated  "));
      // Serial.println(stringValue);
      strcpy (gprsUsername._value, stringValue);
      // Serial.print(F("gprsUsername Number is "));
      Serial.println((gprsUsername._value));
      //write this value to eeprom
      int WriteCounter;
      for (WriteCounter = 0; WriteCounter < stringIndex; WriteCounter++) {
        EEPROM.write(WriteCounter + gprsUsername.startingAddress + 1, gprsUsername._value[WriteCounter]);
      }
      EEPROM.write(WriteCounter + gprsUsername.startingAddress + 1, '\0');
      EEPROM.write(gprsUsername.startingAddress, '\0');
      int readIndex = 0;
#ifdef dbg
      Serial.print(F("reading from EEPROM "));
      for (readIndex = 0; readIndex < stringIndex; readIndex++) {
        char c =  EEPROM.read(gprsUsername.startingAddress + 1 + readIndex);
        if (c == 0)Serial.print('0');
        else Serial.print(char(c));
      }
      Serial.println();
      Serial.println('\n');
#endif
      memset(sms, '\0', sizeof(sms));
      sprintf(sms, "gprsUsername to %s", gprsUsername._value);
      sendSms(admin1._value, sms);
    }
    /////////////////////////////////////////////////////////////////////////////////
    
    else if (has(aux_str, "gprsPassword")) {
      int index = hasStringUpToIndex(aux_str, "gprsPassword"); //locate  the (gprsPassword._value) config
      char stringValue[20];// string for storing the variable being sort.
      int stringIndex = 0;
      while (1) {
        stringValue[stringIndex] = aux_str[index];
        stringIndex++;
        index++;
        if ((index == sizeof(aux_str) | (aux_str[index] == '\0') | (aux_str[index] == ',') | (aux_str[index] == '*') | (aux_str[index] == '#') | (aux_str[index] == 'O'))) {
          stringValue[stringIndex] = '\0';
          break;
        }
      }
      Serial.print(F("gprsPassword No updated  "));
      // Serial.println(stringValue);
      strcpy (gprsPassword._value, stringValue);
      // Serial.print(F("gprsPassword Number is "));
      Serial.println((gprsPassword._value));
      //write this value to eeprom
      int WriteCounter;
      for (WriteCounter = 0; WriteCounter < stringIndex; WriteCounter++) {
        EEPROM.write(WriteCounter + gprsPassword.startingAddress + 1, gprsPassword._value[WriteCounter]);
      }
      EEPROM.write(WriteCounter + gprsPassword.startingAddress + 1, '\0');
      EEPROM.write(gprsPassword.startingAddress, '\0');
      int readIndex = 0;
#ifdef dbg
      Serial.print(F("reading from EEPROM "));
      for (readIndex = 0; readIndex < stringIndex; readIndex++) {
        char c =  EEPROM.read(gprsPassword.startingAddress + 1 + readIndex);
        if (c == 0)Serial.print('0');
        else Serial.print(char(c));
      }
      Serial.println();
      Serial.println('\n');
#endif
      memset(sms, '\0', sizeof(sms));
      sprintf(sms, "gprsPassword set to %s", gprsPassword._value);
      sendSms(admin1._value, sms);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////
     
    else if (has(aux_str, "gprsApn")) {
      int index = hasStringUpToIndex(aux_str, "gprsApn"); //locate  the (gprsApn._value) config
      char stringValue[20];// string for storing the variable being sort.
      int stringIndex = 0;
      while (1) {
        stringValue[stringIndex] = aux_str[index];
        stringIndex++;
        index++;
        if ((index == sizeof(aux_str) | (aux_str[index] == '\0') | (aux_str[index] == ',') | (aux_str[index] == '*') | (aux_str[index] == '#') | (aux_str[index] == 'O'))) {
          stringValue[stringIndex] = '\0';
          break;
        }
      }
      Serial.print(F("gprsApn updated  "));
      // Serial.println(stringValue);
      strcpy (gprsApn._value, stringValue);
      // Serial.print(F("gprsApn Number is "));
      Serial.println((gprsApn._value));
      //write this value to eeprom
      int WriteCounter;
      for (WriteCounter = 0; WriteCounter < stringIndex; WriteCounter++) {
        EEPROM.write(WriteCounter + gprsApn.startingAddress + 1, gprsApn._value[WriteCounter]);
      }
      EEPROM.write(WriteCounter + gprsApn.startingAddress + 1, '\0');
      EEPROM.write(gprsApn.startingAddress, '\0');
      int readIndex = 0;
#ifdef dbg
      Serial.print(F("reading from EEPROM "));
      for (readIndex = 0; readIndex < stringIndex; readIndex++) {
        char c =  EEPROM.read(gprsApn.startingAddress + 1 + readIndex);
        if (c == 0)Serial.print('0');
        else Serial.print(char(c));
      }
      Serial.println();
      Serial.println('\n');
#endif
      memset(sms, '\0', sizeof(sms));
      sprintf(sms, "gprsApn set to %s", gprsApn._value);
      sendSms(admin1._value, sms);
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    return 1;
  }
  else if (has(aux_str, "RESET")) {
    Serial.println("Reseting the system");
    digitalWrite(13, HIGH);
    delay(5000);
    digitalWrite(13, LOW);
    return 1;
  }
  else if (has(aux_str, "STATUS")) {
    return 100;
  }
}


void sim800::readATString(bool watchTimeout = true) {
  char c;
  char buffidx = 0;
  unsigned long time = millis();
  memset(aux_str, '\0', sizeof(aux_str));
  while (1) {
    unsigned long newTime = millis();
    if (watchTimeout) {
      // Time out if we never receive a response
      if (newTime - time > 5000) {
        Serial.println(F("GSM rx timeout"));
        break;
      }
    }


    while (Serial2.available()) {
      c = Serial2.read();
      Serial.print(c);
      if (c == -1) {
        aux_str[buffidx] = '\0';
        return;
      }

      if (c == '\n' || c == '\r') {
        continue;
      }

      if ((buffidx == sizeof(aux_str) - 1)) {
        aux_str[buffidx] = '\0';
        //Serial.println(F("\n at buffer "));
        //Serial.println(aux_str);
        return;
      }

      aux_str[buffidx++] = c;
    }
  }
}


void sim800::readSMS(int _location) {
  int answer = 0;
  int x;
  delay(500);

  //at+cmgf
  sendATcommand("AT+CMGF=1", "OK", 1000);    // sets the SMS mode to text

  // at+cpms

  sendATcommand("AT+CPMS=\"SM\",\"SM\",\"SM\"", "OK", 1000);    // selects the memory

  delay(1000);
  char aux_str[30];
  Serial2.flush();
  // read sms from a location
  sprintf(aux_str, "%s%d", "AT+CMGR=", _location);
  answer = sendATcommand(aux_str, "+CMGR:", 2000);    // reads sms in location


  if (answer == 1)
  {
    answer = 0;
    unsigned long start = millis();
    while (!Serial2.available()) {
      unsigned long time = millis();
      if (time - start > 10000)break;
    }
    unsigned long startNow = millis();
    while (Serial2.read() != '\n') {
      unsigned long timeNow = millis();
      if ((timeNow - start) > 2000)break;
    }
    readATString();
    Serial.println(F("sms content"));
    Serial.println(aux_str);
    if (Serial2.available())Serial.println(F("Lost"));
    else Serial.println(F("buffer empty\n\r"));

    sprintf(aux_str, "%s%d", "AT+CMGD=", _location);
    answer = sendATcommand(aux_str, "OK", 2000);
    if (answer == 1) {
      Serial.print(F("SMS deleted location "));
      Serial.println(_location);
    }
    else Serial.println(F("Deleting sms fail"));
  }
  else
  {
    Serial.print(F("NO SMS in location "));
    Serial.println(_location);
  }
}


int  sim800::NewSMSpresent() {
  int  answer = sendATcommand("AT+CMGL=\"REC UNREAD\",1", "+CMGL:", 5000); // check for new sms
  return answer;
}





