///////////////////////////////////////////////////////
struct settings {
  int startingAddress;// sets the eeprom address from which to bwgin reading
  unsigned char isConfigured;// flag for configuration
  char _value[30];// stores the VALUE of the various PARAMETERS
};
settings server        = {80, 50, "193.70.38.71"};
settings serverPort    = {200, 51, "1883"};
settings deviceId      = {320, 52, "TestDevice"};

settings admin1        = {400, 53, "0724540103"};

settings gprsUsername = {500, 56, "saf"};
settings gprsPassword = {600, 57, "data"};
settings gprsApn      = {700, 58, "safaricom"};

void updateFromEprom() {
  char _buffer1[20];
  char _buffer2[20];
  char _buffer3[40];
  char _buffer4[20];
  char _buffer5[20];
  char _buffer6[20];
  char _buffer7[20];

  //READ server IP adress
  server.isConfigured = EEPROM.read(server.startingAddress); // read the server flag
  if (server.isConfigured == 0xff) // virgin machine
    Serial.println(F("server number not configured"));
  else if (server.isConfigured == '\0') { // server number is set
    // update the server ip address  no coz its configured

    int x = 0;
    char c;
    do {
      c = EEPROM.read(server.startingAddress + 1 + x);
      _buffer1[x] = c;
      x++;
    }
    while ((c != 0) | (x >= 30) | (c != '\0'));

    _buffer1[x] = '\0';
    strcpy(server._value, _buffer1);
    strcpy(MQTTHost, server._value);
    // Serial.print(F("mqtt host IP is "));
    //Serial.println(MQTTHost);

  }
  else Serial.print(F("EEPROM IS CORRUPT"));

  //READ serverPort value
  serverPort.isConfigured = EEPROM.read(serverPort.startingAddress); // read the serverPort flag
  if (serverPort.isConfigured == 0xff) // virgin machine
    Serial.println(F("serverPort number not configured"));
  else if (serverPort.isConfigured == '\0') { // serverPort number is set
    // update the serverPort ip address  no coz its configured
    int x = 0;
    char c;
    do {
      c = EEPROM.read(serverPort.startingAddress + 1 + x);
      _buffer2[x] = c;
      x++;
    }
    while ((c != 0) | (x >= 30) | (c != '\0'));

    _buffer2[x] = '\0';
    strcpy(serverPort._value, _buffer2);
    strcpy(MQTTPort, serverPort._value);
    //Serial.print(F("mqtt host port is "));
    //Serial.println(MQTTPort);

  }
  else Serial.println("EEPROM IS CORRUPT");


  //READ deviceId value
  deviceId.isConfigured = EEPROM.read(deviceId.startingAddress); // read the deviceId flag
  if (deviceId.isConfigured == 0xff) // virgin machine
    Serial.println(F("deviceId  not configured"));
  else if (deviceId.isConfigured == '\0') { // deviceId number is set
    // update the deviceId ip address  no coz its configured
    int x = 0;
    char c;
    do {
      c = EEPROM.read(deviceId.startingAddress + 1 + x);
      _buffer3[x] = c;
      x++;
    }
    while ((c != 0) | (x >= 30) | (c != '\0'));

    _buffer3[x] = '\0';
    strcpy(deviceId._value, _buffer3);
    strcpy(MQTTUsername, deviceId._value);
    // Serial.print(F("Device ID is "));
    //Serial.println(MQTTUsername);

  }
  else Serial.println("EEPROM IS CORRUPT");
  ////////////////////////////////////////////////////////////////////////////////////////
  //READ admin1 value
  admin1.isConfigured = EEPROM.read(admin1.startingAddress); // read the admin1 flag
  if (admin1.isConfigured == 0xff) // virgin machine
    Serial.println(F("admin1  not configured"));
  else if (admin1.isConfigured == '\0') { // admin1 number is set
    // update the admin1 ip address  no coz its configured
    int x = 0;
    char c;
    do {
      c = EEPROM.read(admin1.startingAddress + 1 + x);
      _buffer3[x] = c;
      x++;
    }
    while ((c != 0) | (x >= 30) | (c != '\0'));

    _buffer3[x] = '\0';
    strcpy(admin1._value, _buffer3);
    strcpy(admin, admin1._value);
    // Serial.print(F("Device ID is "));
    //Serial.println(admin);

  }
  else Serial.println("EEPROM IS CORRUPT");
  ///////////////////////////////////////////////////////////////////////////////////////////////////
  //READ gprsUsername value
  gprsUsername.isConfigured = EEPROM.read(gprsUsername.startingAddress); // read the gprsUsername flag
  if (gprsUsername.isConfigured == 0xff) // virgin machine
    Serial.println(F("gprsUsername  not configured"));
  else if (gprsUsername.isConfigured == '\0') { // gprsUsername number is set
    // update the gprsUsername no coz its configured
    int x = 0;
    char c;
    do {
      c = EEPROM.read(gprsUsername.startingAddress + 1 + x);
      _buffer5[x] = c;
      x++;
    }
    while ((c != 0) | (x >= 30) | (c != '\0'));

    _buffer5[x] = '\0';
    strcpy(gprsUsername._value, _buffer5);
    strcpy(USERNAME, gprsUsername._value);
    // Serial.print(F("Device ID is "));
    //Serial.println(APN);

  }
  else Serial.println("EEPROM IS CORRUPT");
  ///////////////////////////////////////////////////////////////////////////////////
  //READ gprsPassword value
  gprsPassword.isConfigured = EEPROM.read(gprsPassword.startingAddress); // read the gprsPassword flag
  if (gprsPassword.isConfigured == 0xff) // virgin machine
    Serial.println(F("gprsPassword  not configured"));
  else if (gprsPassword.isConfigured == '\0') { // gprsPassword number is set
    // update the gprsPassword  coz its configured
    int x = 0;
    char c;
    do {
      c = EEPROM.read(gprsPassword.startingAddress + 1 + x);
      _buffer6[x] = c;
      x++;
    }
    while ((c != 0) | (x >= 30) | (c != '\0'));

    _buffer6[x] = '\0';
    strcpy(gprsPassword._value, _buffer6);
    strcpy(PASSWORD, gprsPassword._value);
    // Serial.print(F("Device ID is "));
    //Serial.println(MQTTUsername);
  }
  else Serial.println("EEPROM IS CORRUPT");

    //READ gprsApn value
  gprsApn.isConfigured = EEPROM.read(gprsApn.startingAddress); // read the gprsApn flag
  if (gprsApn.isConfigured == 0xff) // virgin machine
    Serial.println(F("gprsApn  not configured"));
  else if (gprsApn.isConfigured == '\0') { // gprsApn number is set
    // update the gprsApn  coz its configured
    int x = 0;
    char c;
    do {
      c = EEPROM.read(gprsApn.startingAddress + 1 + x);
      _buffer7[x] = c;
      x++;
    }
    while ((c != 0) | (x >= 30) | (c != '\0'));

    _buffer7[x] = '\0';
    strcpy(gprsApn._value, _buffer7);
    strcpy(APN, gprsApn._value);
    // Serial.print(F("Device ID is "));
    //Serial.println(APN);
  }
  else Serial.println("EEPROM IS CORRUPT");

  sprintf(sms, "server :%s\nport : %s\nadmin1 :%s\nusername: %s\ngprsUsername :%s\ngprsPassword: %s\ngprsApn: %s", MQTTHost, MQTTPort, admin, MQTTUsername, USERNAME,PASSWORD,APN);
  Serial.println(sms);
  myModem.sendSms(admin, sms);
}
