// This custom version of delay() ensures that the gps object
// is being "fed".
static void smartDelay(unsigned long ms)
{
  unsigned long start = millis();
  do
  {
    while (Serial3.available())
      gps.encode(Serial3.read());
  } while (millis() - start < ms);
}

static void printFloat(float val, bool valid, int len, int prec)
{
  if (!valid)
  {
    while (len-- > 1)
      Serial.print('*');
    Serial.print(' ');
  }
  else
  {
    Serial.print(val, prec);
    int vi = abs((int)val);
    int flen = prec + (val < 0.0 ? 2 : 1); // . and -
    flen += vi >= 1000 ? 4 : vi >= 100 ? 3 : vi >= 10 ? 2 : 1;
    for (int i = flen; i < len; ++i)
      Serial.print(' ');
  }
  smartDelay(0);
}

static void printInt(unsigned long val, bool valid, int len)
{
  char sz[32] = "*****************";
  if (valid)
    sprintf(sz, "%ld", val);
  sz[len] = 0;
  for (int i = strlen(sz); i < len; ++i)
    sz[i] = ' ';
  if (len > 0)
    sz[len - 1] = ' ';
  Serial.print(sz);
  smartDelay(0);
}

char sz[32];
char * getDate(TinyGPSDate &d) {
  if (!d.isValid())
  {
    Serial.print(F("********** "));
  }
  else
  {

    sprintf(sz, "%02d/%02d/%02d", d.month(), d.day(), d.year());
    //Serial.print(sz);
    return sz;
  }
}


char st[32];
char * getTime(TinyGPSTime &t) {
  if (!t.isValid())
  {
    Serial.print(F("******** "));
  }
  else
  {
    sprintf(st, "%02d:%02d:%02d", t.hour(), t.minute(), t.second());
    //Serial.print(st);
    return st;
  }


}
static void printDateTime(TinyGPSDate &d, TinyGPSTime &t)
{
  if (!d.isValid())
  {
    Serial.print(F("********** "));
  }
  else
  {
    char sz[32];
    sprintf(sz, "%02d/%02d/%02d ", d.month(), d.day(), d.year());
    Serial.print(sz);
  }

  if (!t.isValid())
  {
    Serial.print(F("******** "));
  }
  else
  {
    char sz[32];
    sprintf(sz, "%02d:%02d:%02d ", t.hour(), t.minute(), t.second());
    Serial.print(sz);
  }

  printInt(d.age(), d.isValid(), 5);
  smartDelay(0);
}

static void printStr(const char *str, int len)
{
  int slen = strlen(str);
  for (int i = 0; i < len; ++i)
    Serial.print(i < slen ? str[i] : ' ');
  smartDelay(0);
}


void getLocation() {
  smartDelay(1000);
 Serial.print("Satelites ");
  if (gps.satellites.isValid())satelites = gps.satellites.value();
  Serial.print(" Latitude ");
  if (gps.location.isValid()) {
    latitude = gps.location.lat();
Serial.print(latitude, 10);
    dtostrf(latitude, 3, 10, latitudeString);
    Serial.print(" latitudeString ");
    Serial.print(latitudeString);
    
  }


  //Serial.print(" Longitude ");
  if (gps.location.isValid()) {
    longitude = gps.location.lng();
   // Serial.print(longitude, 10);
    dtostrf(longitude, 3, 10, longitudeString);
   // Serial.print(" longitudeString  ");
    //Serial.print(longitudeString);
  }


  //Serial.print(" Speed in kph ");
  if (gps.speed.isValid()) {
    speedKph = gps.speed.kmph();
    //Serial.print( speedKph);
  }


  _date = getDate(gps.date);
  //Serial.print(" ");
  //Serial.print(_date);
  //Serial.print(" ");
  _time = getTime(gps.time);
 // Serial.print(_time);
 // Serial.println();

  smartDelay(1000);

  if (millis() > 5000 && gps.charsProcessed() < 10)Serial.println(F("No GPS data received: check wiring"));


  
if (gps.speed.isValid()) sprintf(locations, "{\"latitude\":\"%s\",\"longitude\":\"%s\",\"speed\":\"%d\",\"time\":\"%s\",\"date\":\"%s\",\"satelites\":\"%d\"}", latitudeString, latitudeString,speedKph,_time,_date,satelites);

Serial.println(locations);

}
